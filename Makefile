CC = gcc
CCFLAGS = -Wall -g

all: mainListe mainArbre

mainListe : mainListe.c Liste.h
	$(CC) $(CCFLAGS) -o mainListe mainListe.c

mainArbre : mainArbre.c Arbre.h
	$(CC) $(CCFLAGS) -o mainArbre mainArbre.c

clean :
	rm mainListe mainArbre

archive :
	tar -cvzf ProjetSDA.tar.gz *.c *.h Makefile Doxyfile README *.pdf

doc :
	doxygen
