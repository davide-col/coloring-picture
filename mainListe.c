/**
 * \file mainListe.c
 * \brief Executable qui colorie une image PBM avec une structure liste chaînée
 * \author COLACI, HOCHBERGER
 * \version 1.0
 * \date 2017-2018
*/
#include "Liste.h"

//Entier utile pour initialiser l'aléatoire pour la fonctoin initialiserAleatoire()
int appele_srand = 0;
//Dimsensions lors de l'appel de l'option -g dans le main
int dimensionGenerate;
//Declaration du tableau de stockage d'ensembles
Tabensemble * tableau_ensembles;

//Fonction auxiliaire à valeurAleatoire
void initialiserAleatoire(unsigned int n){
    srand(n);
    appele_srand = 1;
}

/**
* \fn int valeurAleatoire(int min, int max)
* \brief Retourne une valeur aléatoire entre de bornes min et max.
* \param min le minimum
* \param max le maximum
* \pre min<=max.
* \post Retourne un fichre dans les bornes [min,max].
*/
int valeurAleatoire(int min, int max){
    if(appele_srand != 1)
        initialiserAleatoire((unsigned)time(NULL));
    return rand()%(max-min+1) + min;
}

/**
* \fn int isNumber(char chiffre[])
* \brief Teste si le String est une suite de chiffres.
* \param chiffre le String que l'on test.
* \pre chiffre contient la terminaison nulle.
* \post Retourne 1 si vrai, 0 sinon.
*/
int isNumber(char * chiffre){
    int i = 0;
    //checking for negative chiffres
    if (chiffre[0] == '-') i = 1;
    while ( chiffre[i] != 0 ){
        //if (chiffre[i] > '9' || chiffre[i] < '0')
        if (!isdigit(chiffre[i])) return 0;
        i++;
    }
    return 1;
}

/**
* \fn void StockEnsemble(Ensemble e)
* \brief Stock l'ensemble entré en argument dans tableau_ensembles.
* \param e l'ensemble.
* \post tableau_ensembles contient l'ensemble.
*/
void StockEnsemble(Ensemble e){
  tableau_ensembles->tab[e->head->i][e->head->j]=e;
}

/**
* \fn Ensemble SearchEnsemble(Objet x)
* \brief Retourne le représentant de Sx.
* \param x l'objet.
* \pre x a un représentant.
* \post retourne l'ensemble de x si trouvé, null sinon.
*/
Ensemble SearchEnsemble(Objet x){
  if(tableau_ensembles->tab[x->i][x->j]->head==x->rep){
    return tableau_ensembles->tab[x->i][x->j];
  }
  else return NULL;
}

/**
* \fn void DeleteEnsemble(Objet rep)
* \brief Supprime l'ensemble de tableau_ensembles.
* \param x l'objet.
* \post Sx est supprimé de tableau_ensembles.
*/
void DeleteEnsemble(Objet x){
  free(tableau_ensembles->tab[x->i][x->j]);
  tableau_ensembles->tab[x->i][x->j]=NULL;
}

/**
* \fn Objet FindSet(Objet x)
* \brief Retourne le représentant de x.
* \param x l'objet.
* \pre Son représentant existe.
* \post Représentant non NULL.
*/
Objet FindSet(Objet x){
  return x->rep;
}

/**
* \fn Ensemble MakeSet(Objet x)
* \brief Crée un nouvel ensemble Sx contenant uniquement x.
* \param x l'objet.
* \pre x contient les pixels.
* \post Sx contient uniquement x.
*/
Ensemble MakeSet(Objet x){
  //Création de Sx
  Ensemble Sx = malloc (sizeof(struct ensemble));
  Sx->head=x;
  Sx->tail=x;
  Sx->cardinal=1;
  StockEnsemble(Sx);
  return Sx;
}

/**
* \fn Ensemble Union (Objet x, Objet y)
* \brief Crée un nouvel ensemble Sxy par l'union de Sx et Sy.
* \param x et y les objets des ensembles Sx et Sy.
* \pre Les ensembles Sx et Sy sont disjoints.
* \post Sxy contient Sx et Sy.
*/
Ensemble Union (Objet x, Objet y){
  Ensemble Sx;
  Ensemble Sy; //Ensemble Sx
  if((Sx=SearchEnsemble(x))==NULL){ //Recherche de Sx
    fprintf(stderr, "Union ne trouve pas l'ensemble Sx\n");
    exit(1);
  }
  if((Sy=SearchEnsemble(y))==NULL){ //Recherche de Sy
    fprintf(stderr, "Union ne trouve pas l'ensemble Sy\n");
    exit(1);
  }
  //Variables temporelles
  Objet xRep=FindSet(Sx->head);
  Objet yRep=FindSet(Sy->head);
  Objet ytmp=yRep;
  Objet xtmp=xRep;
  int Sxcardinal=Sx->cardinal;
  int Sycardinal=Sy->cardinal;
  Objet Sxtail=Sx->tail;
  Objet Sytail=Sy->tail;
  //Suppression des ensembles Sx et Sy
  DeleteEnsemble(Sx->head); //Suppression de Sx
  DeleteEnsemble(Sy->head);
  //Creation de l'ensemble Sxy
  Ensemble Sxy=malloc(sizeof(struct ensemble));
  //On modifie les pointeurs vers le représentant que pour le plus
    //petit ensemble Sx ou Sy
  if(Sxcardinal>=Sycardinal){ //Si l'ensemble Sx est plus grand que Sy
    Sxtail->next=yRep; //Union des 2 ensembles
    //Changement du pointeur vers le representant
    while(ytmp->next!=NULL){
      ytmp->rep=xRep;
      ytmp->i=xRep->i;
      ytmp->j=xRep->j;
      ytmp=ytmp->next;
    }
    ytmp->rep=xRep; //Dernier element y
    ytmp->i=xRep->i;
    ytmp->j=xRep->j;
    //Replace les pointeurs de Sxy
    Sxy->head=xRep;
    Sxy->cardinal=Sxcardinal+Sycardinal;
    Sxy->tail=ytmp;
  }
  else{ //Si l'ensemble Sy est plus grand que Sx
    Sytail->next=xRep; //Union des 2 ensembles
    //Changement du pointeur vers le representant
    while(xtmp->next!=NULL){
      xtmp->rep=yRep;
      xtmp->i=yRep->i;
      xtmp->j=yRep->j;
      xtmp=xtmp->next;
    }
    xtmp->rep=yRep; //Dernier element y
    xtmp->i=yRep->i;
    xtmp->j=yRep->j;
    //Replace les pointeurs de Sxy
    Sxy->head=yRep;
    Sxy->cardinal=Sycardinal+Sxcardinal;
    Sxy->tail=xtmp;
  }
  StockEnsemble(Sxy); //Stockage de Sxy
  return Sxy;
}



/**
* \fn Image * Read(char * filepath)
* \brief Lis un fichier entré en paramètre.
* \param filepath le chemin.
* \pre Le fichier entré existe, et est une image de type PBM.
* \post L'Image (type Image) retournée contient les dimensions et les pixels de l'image.
*/
Image * Read(char * filepath){
  printf("Read ");
  fflush(stdout);
  char magic[128] = "";
  char c;
  //Ouverture de l'image
  FILE *fd = fopen(filepath, "r");
  if (fd == NULL) {
      perror("Failed to open file");
      exit (1);
  }
  int i=0;
  int j=0;
  //Lecture et sauvegarde du numéro magique
  while(((c=getc(fd))!=EOF)&&(c!='\n')&&(i<128)){
    if(c=='#') while(getc(fd)!='\n'); //Si c'est un commentaire on ignore la ligne
    magic[i]=c;
    i++;
  }
  //Si le numéro magique ne correspond pas à "P1" on quitte
  if (strcmp(magic,"P1")!=0){
    fprintf(stderr,"Le type du fichier est incompatible !\n");
    exit(1);
  }
  char hauteurString[1024] = "";
  char largeurString[1024] = "";
  int hauteur;
  int largeur;
  i=0;
  j=0;
  int hauteurOK=0;
  //Lecture de largeur et hauteur
  while(((c=getc(fd))!=EOF)&&(c!='\n')&&(j<1024)&&(i<1024)){
    if(c=='#') while(getc(fd)!='\n'); //Si c'est un commentaire on ignore la ligne
    if(c==' '){ //Apres l'espace on trouve la hauteur
      hauteurOK=1;
      continue;
    }
    //Tant que l'espace n'est pas parcouru on stock dans largeurString
    if(hauteurOK==0){
      largeurString[j]=c;
      j++;
    }
    //Sinon on stock dans hauteurString
    else{
      hauteurString[i]=c;
      i++;
    }
  }
  //Conversion de char * en int
  hauteur=atoi(hauteurString);
  largeur=atoi(largeurString);
  //Allocation du tableau bidimensionel
  //Allocation 1e dimension
  Pixel ** tab = (Pixel **)malloc ( sizeof(Pixel *)  *  hauteur);
  //Allocation 2e dimension
  for ( i = 0 ; i < hauteur ; i++){
    tab[i] = (Pixel *)malloc (sizeof(Pixel) * largeur);
  }
  //Allocation dest tableaux Pixel
  for ( i = 0 ; i < hauteur ; i++){
    for ( j = 0 ; j < largeur ; j++){
      tab[i][j] = (Pixel)malloc (sizeof(int)*3);
    }
  }
  i=0;
  j=0;
  //Lecture et sauvegarde des pixels de l'image
  if(c=='#') while(getc(fd)!='\n'); //Si c'est un commentaire on ignore la ligne
  //Lecture des pixels
  while(((c=getc(fd))!=EOF)&&(i<(largeur*hauteur))){
    if(c=='\n'){
      continue;
    }
    if(c=='1'){ //Si c'est un pixel noir
      tab[i][j][0]=0;
      tab[i][j][1]=0;
      tab[i][j][2]=0;
    }
    if(c=='0'){ //Si c'est un pixel blanc
      tab[i][j][0]=255;
      tab[i][j][1]=255;
      tab[i][j][2]=255;
    }
    if((j+1)%largeur==0){
      i++;
      j=0;
      continue;
    }
    j++;
  }
  //Stockage dans structure Image
  Image * picture=malloc(sizeof(Image));
  picture->hauteur=hauteur;
  picture->largeur=largeur;
  picture->tab=tab;
  fclose(fd);
  printf("done !\n");
  return picture;
}


/**
* \fn Tabobjet * MakeObjectsTab (Image * I)
* \brief Crée et alloue un tableau d'objets avec les pixels de I.
* \param I l'image.
* \pre hauteur*largeur = nombre de pixels.
* \post Tabobjet contient les objets qui eux contiennent les pixels.
*/
Tabobjet * MakeObjectsTab (Image * I){
  printf("MakeObjectsTab ");
  fflush(stdout);
  Tabobjet * O = (Tabobjet *) malloc(sizeof(Tabobjet));
  O->hauteur=I->hauteur;
  O->largeur=I->largeur;
  int hauteur=I->hauteur;
  int largeur=I->largeur;
  int i,j;
  //Allocation d'un tableau 2D d'Objets
  //Allocation 1e dimension
  Objet ** Otab = (Objet **)malloc ( sizeof(Objet *)  *  hauteur);
  //Allocation 2e dimension
  for (i = 0 ; i < hauteur ; i++){
    Otab[i] = (Objet *)malloc (sizeof(Objet) * largeur);
  }
  for (i = 0 ; i < hauteur ; i++){
    for (j = 0 ; j < largeur ; j++){
      //Creation et allocation d'un Objet
      Otab[i][j] = (Objet)malloc (sizeof(struct objet));
      Otab[i][j]->pixel=I->tab[i][j];
      Otab[i][j]->rep=Otab[i][j];
      Otab[i][j]->next=NULL;
      Otab[i][j]->i=i;
      Otab[i][j]->j=j;
      //Si le pixel est blanc on choisit une couleur
      if(isWhite(Otab[i][j]->pixel)){
        //On choisit une couleur différente de blanc ou noir
        //Si la couleur choisie est blanc ou noir on rechoisis une couleur (while)
        while(isWhite(Otab[i][j]->pixel)||isBlack(Otab[i][j]->pixel)){
          Otab[i][j]->pixel[0]=valeurAleatoire(0,255);
          Otab[i][j]->pixel[1]=valeurAleatoire(0,255);
          Otab[i][j]->pixel[2]=valeurAleatoire(0,255);
        }
      }
    }
  }
  //Implémantation des données dans la strucute Image
  O->tab=Otab;
  printf("done !\n");
  return O;
}

/**
* \fn int PixelOK(Pixel p)
* \brief Test si le pixel est conforme, i.e. si le pixel est compris entre 0 et 255.
* \param p le pixel.
* \pre Le pixel est initialisé.
* \post renvoie 1 si le pixel est conforme, 0 sinon.
*/
int PixelOK(Pixel p){
  if (p[0]>255||p[0]<0||p[1]>255||p[1]<0||p[2]>255||p[2]<0){
    return 0;
  }
  else{
    return 1;
  }
}

/**
* \fn Write(Image * picture, char * nom)
* \brief Ecris une image au format PPM.
* \param picture l'image que l'on a traité.
* \param nom le nom du fichier qu'on l'on souhaite attribuer.
* \pre hauteur*largeur = nombre de pixels.
* \post l'image créée est de type PPM et conforme.
*/
void Write(Image * picture, char * nom){
  printf("Write ");
  fflush(stdout);
  char * name = malloc(1024*sizeof(char));
  if (nom==NULL){
    strcpy (name,"picture.ppm");
  }
  else{
    strcpy (name,nom);
  }
  FILE *fp=fopen(name,"w");
  fprintf(fp,"P3\n");
  fprintf(fp,"%d %d\n",picture->largeur,picture->hauteur);
  fprintf(fp,"255\n");
  free(name);
  int i,j;
  int compteur=0;
  for(i=0;i<picture->hauteur;i++){
    for(j=0;j<picture->largeur;j++){
      if(!PixelOK(picture->tab[i][j])){ //Test si les valeurs sont conformes (<=255 et >=0)
        perror("Write : La structure des pixels n'est pas respectée !");
        exit(1);
      }
      fprintf(fp," %d ",picture->tab[i][j][0]);
      compteur++;
      if (compteur%12==0){
        fprintf(fp,"\n");
      }
      fprintf(fp," %d ",picture->tab[i][j][1]);
      compteur++;
      if (compteur%12==0){
        fprintf(fp,"\n");
      }
      fprintf(fp," %d ",picture->tab[i][j][2]);
      compteur++;
      if (compteur%12==0){
        fprintf(fp,"\n");
      }
    }
  }
  fclose(fp);
  printf("done !\n");
}

/**
* \fn void SetColors ( Tabobjet * T)
* \brief Attribue la couleur du représentant de l'ensemble à chaque pixel de l'image
* \param T le tableau d'objets.
* \pre T a des dimensions justes
* \post Les seront ceux du représentant
*/
void SetColors (Tabobjet * T){
  printf("SetColors ");
  fflush(stdout);
  int i,j;
  for(i=0;i<T->hauteur;i++){
    for(j=0;j<T->largeur;j++){
      //On retient la couleur du représentent de l'Objet
      T->tab[i][j]->pixel[0]=FindSet(T->tab[i][j])->pixel[0];
      T->tab[i][j]->pixel[1]=FindSet(T->tab[i][j])->pixel[1];
      T->tab[i][j]->pixel[2]=FindSet(T->tab[i][j])->pixel[2];
    }
  }
  printf("done !\n");
}


/**
* \fn Image * Generate(int n, int m, char * nom)
* \brief Génère une image aléatoire avec des pixels soit noirs soit blancs.
* \param n la hauteur de l'image.
* \param m la largeur de l'image.
* \param nom le nom du fichier qu'on l'on souhaite attribuer.
* \pre n et m sont positifs.
* \post Image de retour est de dimension n,m.
*/
Image * Generate(int n, int m, char * nom){
  printf("Generate ");
  if(nom==NULL){
    nom="aléatoire.ppm";
  }
  int i,j;
  //Création d'un tableau de dimension m * n
  Image * picture=malloc(sizeof(Image));
  picture->hauteur=n;
  picture->largeur=m;
  int hauteur = n;
  int largeur = m;
  //Allocation 1e dimension
  Pixel ** tab = (Pixel **)malloc ( sizeof(Pixel *)  *  hauteur);
  //Allocation 2e dimension
  for ( i = 0 ; i < hauteur ; i++){
    tab[i] = (Pixel *)malloc (sizeof(Pixel) * largeur);
  }
  //Allocation dest tableaux Pixel
  for ( i = 0 ; i < hauteur ; i++){
    for ( j = 0 ; j < largeur ; j++){
      tab[i][j] = (Pixel)malloc (sizeof(int)*3);
    }
  }
  //on choisit une valeur noire ou blanche pour le pixel
  for (i=0;i<m;i++){
    for(j=0;j<n;j++){
      int val=valeurAleatoire(0,1);
      if(val==1){
        val=255;
      }
      tab[i][j][0]=val;
      tab[i][j][1]=val;
      tab[i][j][2]=val;
    }
  }
  picture->tab=tab;
  printf("done !\n");
  //Write(picture,nom); //Ecriture de l'image
  return picture;
}

/**
* \fn int isWhite(Pixel p)
* \brief Test si le pixel est blanc,
* \param p le pixel.
* \pre p contient 3 pixels de valeur entre 0 et 255.
* \post retourne 1 si vrai, sinon.
//Postcondition : retourne 1 si vrai, 0 sinon.
*/
int isWhite(Pixel p){
  return ((p[0]==255)&&(p[1]==255)&&(p[2]==255));
}

/**
* \fn int isBlack(Pixel p)
* \brief Test si le pixel est noir.
* \param p le pixel.
* \pre p contient 3 pixels de valeur entre 0 et 255.
* \post retourne 1 si vrai, sinon.
//Postcondition : retourne 1 si vrai, 0 sinon.
*/
int isBlack(Pixel p){
  return ((p[0]==0)&&(p[1]==0)&&(p[2]==0));
}

/**
* \fn void FreeImage(Image * I)
* \brief Free une Image
* \param I l'image.
* \pre I n'a pas encore été free, les dimensions sont justes.
* \post I est free.
*/
void FreeImage(Image * I){
  printf("FreeImage ");
  fflush(stdout);
  int i,j;
  for(i=0;i<I->hauteur;i++){
    for(j=0;j<I->largeur;j++){
      free(I->tab[i][j]); //Free pixels du tableau
    }
  }
  for(i=0;i<I->hauteur;i++){
    free(I->tab[i]); //Free lignes tableau
  }
  free(I->tab); //Free tableau
  free(I); //Free image
  printf("done !\n");
}

/**
* \fn void FreeObjects(Tabobjet * I)
* \brief Free une tableau d'objets
* \param T le tableau d'objets.
* \pre T n'a pas encore été free, les dimensions sont justes.
* \post T est free.
*/
void FreeObjects(Tabobjet * T){
  printf("FreeObjects ");
  fflush(stdout);
  int i,j;
  for(i=0;i<T->hauteur;i++){
    for(j=0;j<T->largeur;j++){
      free(T->tab[i][j]); //Free Objets
    }
  }
  for(i=0;i<T->hauteur;i++){ //Free lignes tableau
    free(T->tab[i]);
  }
  free(T->tab); //Free tableau
  free(T); //Free Image
  printf("done !\n");
}

/**
* \fn void FreeSets()
* \brief Free tableau_ensembles.
* \pre les dimensions sont justes.
* \post tableau_ensembles est free.
*/
void FreeSets(){
  printf("FreeSets ");
  fflush(stdout);
  int i,j,compteur=0;
  for(i=0;i<tableau_ensembles->hauteur;i++){
    for(j=0;j<tableau_ensembles->largeur;j++){
      if(tableau_ensembles->tab[i][j]!=NULL){
        free(tableau_ensembles->tab[i][j]); //Free Ensemble
        compteur++;
      }
    }
  }
  for(i=0;i<tableau_ensembles->hauteur;i++){ //Free lignes tableau
    free(tableau_ensembles->tab[i]);
  }
  free(tableau_ensembles->tab); //Free tableau
  free(tableau_ensembles); //Free Image
  printf("done !\n");
  printf("Number of Sets/Colors : %d\n",compteur);
}

/**
* \fn void MakeSetsTab(Tabobjet * T)
* \brief Crée et alloue tableau_ensembles.
* \post tableau_ensembles est alloué.
*/
void MakeSetsTab(Tabobjet * T){
  printf("MakeSetsTab ");
  tableau_ensembles = (Tabensemble *) malloc(sizeof(Tabensemble));
  tableau_ensembles->hauteur=T->hauteur;
  tableau_ensembles->largeur=T->largeur;
  int hauteur=T->hauteur;
  int largeur=T->largeur;
  int i,j;
  //Allocation d'un tableau 2D d'Objets
  //Allocation 1e dimension
  tableau_ensembles->tab = (Ensemble **)malloc ( sizeof(Ensemble *)  *  hauteur);
  //Allocation 2e dimension
  for (i = 0 ; i < hauteur ; i++){
    tableau_ensembles->tab[i] = (Ensemble *)malloc (sizeof(Ensemble) * largeur);
  }
  for (i = 0 ; i < hauteur ; i++){
    for (j = 0 ; j < largeur ; j++){
      //Creation et allocation d'un Ensemble
      tableau_ensembles->tab[i][j] = NULL;
    }
  }
  printf(" done !\n");
}

/**
* \fn void Coloriage (char * filepath)
* \brief Fonction principale qui colorie une image initialement en noir et blanc.
* \param filepath le chemin de l'image PBM à colorier.
* \pre fichier filepath exite de type PBM.
* \post colorie le fichier et l'écris en PPM (couleurs.ppm).
*/
void Coloriage (char * filepath){
  printf("\n\tStart coloriage\n");
  Image * ppm;
  if(filepath!=NULL){
    ppm = Read(filepath); //Lecture de l'image
  }
  else{
    printf("Test avec Generate : Image %d x %d\n",dimensionGenerate,dimensionGenerate);
    ppm = Generate(dimensionGenerate,dimensionGenerate,NULL);
  }
  Tabobjet * TO = MakeObjectsTab(ppm); //Conversion en tableau d'Objets
  MakeSetsTab(TO); //Creation du stockage des ensembles
  int i,j=0;
  for(i=0;i<TO->hauteur;i++){
    printf("\rColoriage... %d%% ",(int)(((float)i/(float)(TO->hauteur-1))*100));
    fflush(stdout);
    for (j=0;j<TO->largeur;j++){
      if(!isBlack(TO->tab[i][j]->pixel)){ //Si le pixel n'est pas noir
        MakeSet(TO->tab[i][j]);
        //Si le voisin de gauche exite, s'il n'est pas noir, et si son représentant n'est pas identique
        //(donc disjoint dans cet algotithme), On fait l'union des deux ensembles
        if((j>0)&&!isBlack(TO->tab[i][j-1]->pixel)&&(FindSet(TO->tab[i][j-1])!=FindSet(TO->tab[i][j]))){
          Union(TO->tab[i][j-1],TO->tab[i][j]);
        }
        //De même pour le voisin haut
        if((i>0)&&!isBlack(TO->tab[i-1][j]->pixel)&&(FindSet(TO->tab[i-1][j])!=FindSet(TO->tab[i][j]))){
          Union(TO->tab[i-1][j],TO->tab[i][j]);
        }
      }
    }
  }
  printf("done !\n");
  SetColors(TO); //Ecriture du tableau d'objets dans tableau Image
  Write(ppm,"couleurs.ppm"); //Ecriture de l'image couleurs.ppm
  FreeImage(ppm); //Suppression de l'Image
  FreeObjects(TO); //Suppression du tableau d'Objets
  FreeSets(); //Suppression du tableau d'Objets
  printf("\tEnd coloriage\n");
}

/*! \mainpage Home
 * \section Makefile
 * \subsection compilation Compilation:
 * - $make
 * \subsection archive Création de l'archive:
 * - $make archive
 * \subsection doxygen Création des fichiers doxygen:
 * - $make doc
 * \subsection clean Nettoyage des fichiers exécutables:
 * - $make clean
 *
 * \section mainListe
 *
 * \subsection execution Execution
 * - ./mainListe "Image" : coloriage d'une image PBM donnée en entrée
 * - ./mainListe -g "taille" : coloriage d'une image créée aléatoirement de taille "taille" (int) avec option -g
 *
 * \section mainArbre
 *
 * \subsection execution Execution
 * - ./mainArbre "Image" : coloriage d'une image PBM donnée en entrée
 * - ./mainArbre -g "taille" : coloriage d'une image créée aléatoirement de taille "taille" (int) avec option -g
 *
 */
int main(int argc, char *argv[]) {
  int c;
  int gflag=0;
  clock_t start_t, end_t;
  double total_t;
  while ((c = getopt (argc, argv, "g:"))!=-1){ //Recherche des options avec getopt
      switch (c){
        case '?':
          if (optopt == 'g'){
            fprintf (stderr, "Veuillez entrer un argument pour -g <dimension> !\n");
            exit(1);
            break;
          }
          else {
            fprintf(stderr, "Unknown option -%c.\n",optopt);
            exit(1);
            break;
          }
        case 'g':
          //Si l'option -g est activée on enregistre l'argument dimension
          gflag = 1;
          if(!isNumber(argv[2])){
            fprintf (stderr, "The dimension of -g has to be an integer !\n");
            exit(1);
          }
          dimensionGenerate=atoi(argv[2]);
          break;
        default:
          fprintf (stderr, "getopt\n");
          exit(1);
      }
  }
  //Si l'option -g est désactivée on lance la fonction Coloriage avec un fichier PPM
    //entré en argument lors de l'exécution
  if (gflag==0){
    if(argc!=2){ //Test le nombre d'arguments
      fprintf(stderr,"Veuillez entrer un argument de la forme : <fichier> ou l'option -g (Génère une image aléatoire)\n");
      exit(1);
    }
    char * filepath = malloc(1000*sizeof(char));
    strcpy(filepath,argv[1]);
    Coloriage(filepath); //Lancement de la fonction Coloriage
    free(filepath);
  }
  //Si l'option -g est activée on lance la fonction Coloriage avec la génération aléatoire
    //d'une image carrée avec une unique dimension entrée en argument lors de l'exécution
  else{
    if(argc!=3){ //Test le nombre d'arguments
      fprintf(stderr,"Veuillez entrer un argument de la forme : <fichier> ou l'option -g (Génère une image aléatoire)\n");
      exit(1);
    }
    FILE *file = fopen("mainListe.time", "a");
    start_t = clock (); //Départ du compteur
    Coloriage(NULL); //Lancement de la fonction Coloriage avec l'option NULL : Generate
    end_t=clock(); //Arrêt du compteur
    total_t = ((double)(end_t-start_t))/(double)CLOCKS_PER_SEC;
    //Mesure le temps mis par le processus
    fprintf(file,"%d\t%f\n",dimensionGenerate*dimensionGenerate,total_t);
    fclose(file);
  }
  return 0;
}
