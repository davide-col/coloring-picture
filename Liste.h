/**
 * \file Liste.h
 * \brief fichier .h pour les fonctions mainListe.c
 * \author COLACI, HOCHBERGER
 * \version 1.0
 * \date 2017-2018
 */
//Version Liste
#include "stdio.h"
#include "stdlib.h"
#include "math.h"
#include "string.h"
#include "time.h"
#include "unistd.h"
#include "ctype.h"
#include "fcntl.h"

//Tableau Pixel : [R,G,B]
typedef int * Pixel;

//Structure Objet : Liste chaînée
typedef struct objet {
  struct objet * rep; //Pointeur vers l'Objet représentant
  struct objet * next; //Pointeur vers l'Objet suivant
  Pixel pixel; //Tableau Pixel : [R,G,B]
  int i; //Coordonnée i de l'objet
  int j; //Coordonnée j de l'objet
} * Objet;

//Structure Ensemble : Liste chaînée
typedef struct ensemble{
  Objet head; //Pointeur vers l'Objet représentant
  Objet tail; //Pointeur vers l'Objet le plus éloigné du représentant
  int cardinal; //Nombre d'objets dans l'ensemble
} * Ensemble;

typedef struct image{
  Pixel ** tab; //Tableau 2D d'éléments Pixel : [R,G,B]
  int hauteur; //Hauteur Image
  int largeur; //Largeur Image
}Image;

//Structure tableau permettant de stocker les Objets
typedef struct tabobjet{
  Objet ** tab; //Tableau 2D d'éléments Objet
  int hauteur; //Hauteur tableau
  int largeur; //Largeur tableau
}Tabobjet;

//Structure tableau permettant de stocker les Objets
typedef struct tabensemble{
  Ensemble ** tab; //Tableau 2D d'éléments Objet
  int hauteur; //Hauteur tableau
  int largeur; //Largeur tableau
}Tabensemble;

//Fonction auxiliaire à valeurAleatoire
void initialiserAleatoire(unsigned int);
//Retourne une valeur aléatoire entre de bornes min et max
int valeurAleatoire(int min, int max);
//Teste si le String est une suite de chiffres
int isNumber(char *);
//Stock l'ensemble entré en argument dans tableau_ensembles
void StockEnsemble(Ensemble);
//Retourne le représentant de Sx
Ensemble SearchEnsemble(Objet);
//Supprime l'ensemble de tableau_ensembles.
void DeleteEnsemble(Objet);
//Retourne le représentant de x
Objet FindSet(Objet);
//Crée un nouvel ensemble Sx contenant uniquement x
Ensemble MakeSet(Objet);
//Crée un nouvel ensemble Sxy par l'union de Sx et Sy
Ensemble Union (Objet, Objet);
//Lis un fichier entré en paramètre
Image * Read(char *);
//Crée et alloue un tableau d'objets
Tabobjet * MakeObjectsTab (Image *);
//Ecris une image au format PPM
void Write(Image *, char *);
//Attribue la couleur du représentant de l'ensemble à chaque pixel de l'image
void SetColors ( Tabobjet *);
//Génère une image aléatoire avec des pixels soit noirs soit blancs
Image * Generate(int n, int m, char * nom);
//Test si le pixel est blanc
int isWhite(Pixel);
//Test si le pixel est noir
int isBlack(Pixel);
//Free une Image
void FreeImage(Image *);
//Free une tableau d'objets
void FreeObjects(Tabobjet *);
//Test si le pixel est conforme, i.e. si le pixel est compris entre 0 et 255.
int PixelOK(Pixel);
//Free tableau_ensembles
void FreeSets();
//Crée et alloue tableau_ensembles
void MakeSetsTab();
//Fonction principale qui colorie une image initialement en noir et blanc
void Coloriage (char *);
