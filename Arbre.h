/**
 * \file Arbre.h
 * \brief fichier .h pour les fonctions mainArbre.c
 * \author COLACI, HOCHBERGER
 * \version 1.0
 * \date 2017-2018
 */
//Version Arbre
#include "stdio.h"
#include "stdlib.h"
#include "math.h"
#include "string.h"
#include "time.h"
#include "unistd.h"
#include "ctype.h"

//Tableau Pixel : [R,G,B]
typedef int * Pixel;

//Structure Objet : Arbre
typedef struct objet {
  struct objet * pere; //Pointeur vers l'Objet père
  int rang; //Rang de l'arbre
  Pixel pixel; //Tableau Pixel : [R,G,B]
} * Objet;

typedef struct image{
  Pixel ** tab; //Tableau 2D d'éléments Pixel : [R,G,B]
  int hauteur; //Hauteur Image
  int largeur; //Largeur Image
}Image;

//Structure tableau permettant de stocker les Objets
typedef struct tabobjet{
  Objet ** tab; //Tableau 2D d'éléments Objet
  int hauteur; //Hauteur tableau
  int largeur; //Largeur tableau
}Tabobjet;

//Fonction auxiliaire à valeurAleatoire
void initialiserAleatoire(unsigned int);
//Retourne une valeur aléatoire entre de bornes min et max
int valeurAleatoire(int min, int max);
//Teste si le String est une suite de chiffres
int isNumber(char *);
//Retrouve l'ensemble dans la liste et le supprimme
void DeleteEns(Objet);
//Retourne le représentant de x
Objet FindSet(Objet);
//Lis un fichier entré en paramètre
Image * Read(char *);
//Permet de stocker les informations d'une image au format PBM
Tabobjet * MakeObjectsTab (Image *);
//Ecris une image au format PPM
void Write(Image *, char *);
//Attribue la couleur du représentant de l'ensemble à chaque pixel de l'image
void SetColors ( Tabobjet *);
//Génère une image aléatoire avec des pixels soit noirs soit blancs
Image * Generate(int n, int m, char * nom);
//Test si le pixel est blanc
int isWhite(Pixel);
//Test si le pixel est noir
int isBlack(Pixel);
//Free une Image
void FreeImage(Image *);
//Free une tableau d'objets
void FreeObjects(Tabobjet *);
//Test si le pixel est conforme, i.e. si le pixel est compris entre 0 et 255.
int PixelOK(Pixel);
//Fonction principale qui colorie une image initialement en noir et blanc
void Coloriage (char *);
